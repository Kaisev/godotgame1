[CmdletBinding()]
param (
    [Parameter(Position = 0)]
    [System.IO.DirectoryInfo]
    $InputPath = '.\',
    [Parameter(Position = 1)]
    [System.IO.DirectoryInfo]
    $OutputPath = '.\.build',
    [Parameter(Position = 2)]
    [bool]
    $Clean = $true
)
$Include = @(
    "*.exe",
    "*.pck",
    "*.otf",
    "*.ttf",
    ".import"
)
if (!$OutputPath.Exists) {
    Write-Host "Creating $($OutputPath.FullName)"
    $OutputPath.Create()
    $Clean = $false
}

if ($Clean) {
    Write-Host "Cleaning $($OutputPath.FullName)"
    Remove-Item -Recurse -Force ($OutputPath.FullName + '*') -Verbose
}

foreach ($Item in $Include) {
    Copy-Item -Recurse -Force -Path ($InputPath.FullName + $Item) -Destination $OutputPath -Verbose
}
pause