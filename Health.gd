extends CanvasLayer

export var max_health = 3
var current_health
var health_array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	health_array.append($Health_Indicator)
	for i in max_health - 1:
		var item = $Health_Indicator.duplicate()
		add_child(item)
		item.position.x += (i + 1) * 50
		health_array.append(item)
	init()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var diff = max_health - current_health
	for i in max_health - current_health:
		health_array[-1 * (i + 1)].play("damage")
func init():
	current_health = max_health
	for item in health_array:
		item.play("filled")
