This is a rudimentary game made to learn the Godot engine

WASD to move, space to fire.

To build:

1. Open project in Godot
2. Export using Windows desktop template, export path ./Game 1.exe
3. Run Make-Release.ps1

The built project will now appear in the .build folder
