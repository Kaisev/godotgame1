extends Area2D

export var speed = 100

signal hit

var velocity = Vector2.ZERO

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.play("default")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.x = speed
	position += velocity


func _on_Rocket_body_entered(body):
	if body.name == "Plane":
		pass
	else:
		speed = 0
		$AnimatedSprite.play("hit")


func _on_Rocket_area_shape_entered(area_rid, area, area_shape_index, local_shape_index):
	if area.name != "Plane" && area.name != "Rocket":
		speed = 0
		$AnimatedSprite.play("hit")
		emit_signal("hit")


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "hit":
		get_parent().queue_free()
		emit_signal("hit")
