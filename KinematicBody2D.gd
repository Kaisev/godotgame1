extends Area2D

export var speed = 800
export var acceleration = 500
export var deceleration = 250 # Must never be higher than the acceleration
export var model_scale = 0.5
export var health = 5
export var iframes = 0.5

var invincible = false
var screen_size
var velocity = Vector2.ZERO
var hit = false

signal hit

var rocketsrc = preload("res://Rocket.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	scale.x = model_scale
	scale.y = model_scale
	$PlaneSprite.play("neutral")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !hit:
		if Input.is_action_pressed("move_right") && velocity.x < speed * Input.get_action_strength("move_right"):
			velocity.x += acceleration
			if $PlaneSprite.animation == "neutral":
				$PlaneSprite.play("boost")
		else:
			if !Input.is_action_pressed("move_right") && $PlaneSprite.animation == "boosting":
				$PlaneSprite.play("neutral")
		if Input.is_action_pressed("move_left") && velocity.x > -1 * speed * Input.get_action_strength("move_left"):
			velocity.x -= acceleration
			if $PlaneSprite.animation == "neutral":
				$PlaneSprite.play("brake")
		else:
			if !Input.is_action_pressed("move_left") && $PlaneSprite.animation == "brake":
				$PlaneSprite.play("neutral")
		if Input.is_action_pressed("move_up") && velocity.y > -1 * speed * Input.get_action_strength("move_up"):
			velocity.y -= acceleration
		if Input.is_action_pressed("move_down") && velocity.y < speed * Input.get_action_strength("move_down"):
			velocity.y += acceleration
	
	# Returns the player to a stop when they are not pressing an input
	# Note that like with real friction this always applies, if the acceleration is lower than the deceleration the player will not be able to overpower it
	velocity.x = decelerate(velocity.x)
	velocity.y = decelerate(velocity.y)
		
	# Keep the player on the screen
	if position.x >= screen_size.x - model_scale * 100 && velocity.x > 0:
		velocity.x = 0
	if position.y >= screen_size.y - model_scale * 100 && velocity.y > 0:
		velocity.y = 0
	if position.x <= 0 + model_scale * 100 && velocity.x < 0:
		velocity.x = 0
	if position.y <= 0 + model_scale * 100 && velocity.y < 0:
		velocity.y = 0

	# Ensure the player is obeying the speed limit
	velocity.x = speedlimit(velocity.x)
	velocity.y = speedlimit(velocity.y)

	# Set the new position
	position += delta * velocity

# Determines if a number is positive or negative
func getinverter(input):
	if input < 0:
		return -1
	else:
		return 1

# Decelerates the player until they have stopped
func decelerate(axis):
	var inverter = getinverter(axis)
	axis = axis * inverter
	
	if axis > 0:
		axis -= deceleration
		if axis < 0:
			axis = 0
	return axis * inverter

# Prevents the player's speed from exceeding the maximum
func speedlimit(axis):
	var inverter = getinverter(axis)
	axis = axis * inverter
	
	if axis > speed:
		axis = speed
	return axis * inverter

# Handles being crashed or hit
func _on_Plane_area_shape_entered(area_rid, area, area_shape_index, local_shape_index):
	if area.name != "Rocket" && !invincible:
		invincible = true
		health -= 1
		if health <= 0:
			acceleration = 0
			$PlaneSprite.play("hit")
			hit = true
		else:
			$PlaneSprite.play("damage")
			yield(get_tree().create_timer(iframes), "timeout")
			invincible = false

# Deletes self once the hit animation is done
func _on_PlaneSprite_animation_finished():
	if $PlaneSprite.animation == "hit":
		emit_signal("hit")
		get_parent().get_node(self.name).queue_free()
	elif $PlaneSprite.animation == "damage":
		$PlaneSprite.animation = "neutral"
	elif $PlaneSprite.animation == "boost":
		$PlaneSprite.play("boosting")
