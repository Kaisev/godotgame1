extends Sprite


var set = false


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !visible && !set:
		offset = Vector2(randi() % 8000 - 4000, randi() % 400 - 200)
	if visible && set == true:
		set = false
