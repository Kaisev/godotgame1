extends Area2D

export var speed = 500
export var dodge_speed = 1000

export var variance = 20
export var acceleration = 10

export var health = 2

var hitters = []

var velocity = Vector2.ZERO
var targetvelocity = Vector2.ZERO

var screen_size

var has_been_visible = false

signal hit


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	screen_size = get_viewport_rect().size
	position.y = randi() % int(screen_size.y) - screen_size.y / 2
	$AnimatedSprite.play("neutral")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !$VisibilityNotifier2D.is_on_screen():
		if has_been_visible:
			queue_free()
	else:
		has_been_visible = true
	velocity.x = -1 * speed
	if randi() % 1000 < variance:
		targetvelocity.y = randi() % (dodge_speed * 2) - dodge_speed

	# Accelerate to speed
	if velocity.y > targetvelocity.y:
		velocity.y -= acceleration
	if velocity.y < targetvelocity.y:
		velocity.y += acceleration
	
	# Speed limit
	if velocity.y > dodge_speed || velocity.y < -1 * dodge_speed:
		var invert = 1
		if velocity.y < 0:
			invert = -1
		velocity.y = invert * dodge_speed
	
	# Prevents object from leaving the screen
	if position.y >= screen_size.y / 2 && velocity.y > 0:
		velocity.y = -1 * velocity.y
		targetvelocity.y = -1 * targetvelocity.y
	if position.y <= -1 * screen_size.y / 2 && velocity.y < 0:
		velocity.y = -1 * velocity.y
		targetvelocity.y = -1 * targetvelocity.y
	position += velocity * delta


func _on_Enemy_area_shape_entered(area_rid, area, area_shape_index, local_shape_index):
	if area.name != "Enemy" && !hitters.has(area_rid):
		health -= 1
		if area.name != "Plane":
			hitters.append(area_rid)
			$AnimatedSprite.play("damage")
		if health <= 0:
			speed = 0
			$CollisionPolygon2D.queue_free()
			$AnimatedSprite.play("hit")

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "hit":
		get_parent().queue_free()
		emit_signal("hit")
	elif $AnimatedSprite.animation == "damage":
		$AnimatedSprite.animation = ("neutral")
