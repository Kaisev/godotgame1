extends Node2D

var rocketsrc = preload("res://Rocket.tscn")
var enemysrc = preload("res://Enemy.tscn")
var planesrc = preload("res://Plane.tscn")

var score = 0

var plane

var fire_down = false
var screen_size

var time = 120

var timer = Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	$PopupMenu.show_modal(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ParallaxBackground.scroll_base_offset.x -= 20
	$ParallaxForeground.scroll_base_offset.x -= 20
	if Input.is_action_pressed("fire"):
		if !fire_down && !$PopupMenu.visible:
			var planescale = plane.get_node("Plane").model_scale
			var rocket = rocketsrc.instance()
			rocket.position = plane.get_node("Plane").position + Vector2(300, 30) * planescale
			rocket.scale.x = planescale
			rocket.scale.y = planescale
			add_child_below_node(get_parent(), rocket)
			fire_down = true
	else:
		fire_down = false
	
	if Input.is_action_pressed("menu"):
		if !$PopupMenu.visible:
			get_tree().paused = true
			$PopupMenu.show_modal(true)
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if plane:
		if plane.get_node("Plane"):
			$Health.current_health = plane.get_node("Plane").health

func _on_Timer_timeout():
	spawn_enemies(4, 0.25)

func spawn_enemies(var count, var frequency):
	for i in count:
		var enemy = enemysrc.instance()
		enemy.position = Vector2(screen_size.x + 50, screen_size.y / 2)
		add_child(enemy)
		enemy.add_to_group("Enemies")
		yield(get_tree().create_timer(frequency), "timeout")
		enemy.get_node("Enemy").connect("hit", self, "_on_Enemy_hit")

func playgame():
	timer.connect("timeout", self, "_on_timer_timeout")
	add_child(timer)
	timer.start(time)
	$ParallaxBackground/ParallaxLayer3.motion_scale.x = 6.8 / time
	$ParallaxBackground/ParallaxLayer3.motion_offset.x = 0
	score = 0
	$RichTextLabel.text = str(score)
	for child in get_tree().get_nodes_in_group("Enemies"):
		child.queue_free()
	#$ParallaxBackground.scroll_offset.x = 0
	#$ParallaxForeground.scroll_offset.x = 0
	plane = planesrc.instance()
	add_child(plane)
	plane.get_node("Plane").position = Vector2(100, screen_size.y /2)
	$Timer.start()
	plane.get_node("Plane").connect("hit", self, "_on_Plane_hit")
	plane.get_node("Plane").health = $Health.max_health
	$Health.init()
	

func _on_Play_pressed():
	$PopupMenu.hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if get_tree().paused == true:
		get_tree().paused = false
	else:
		playgame()

func _on_Quit_pressed():
	get_tree().quit()
	
func _on_Plane_hit():
	$Timer.stop()
	$PopupMenu.show_modal(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
func _on_Enemy_hit():
	score += 1
	$RichTextLabel.text = str(score)

func _on_timer_timeout():
	remove_child(timer)
	$ParallaxBackground/ParallaxLayer3.motion_scale.x = 0
	$ParallaxBackground/ParallaxLayer3.motion_offset.x = screen_size.x - 10000
