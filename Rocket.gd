extends 
export var speed = 100
export var acceleration = 1
export var drop = 0

var velocity = Vector2.ZERO

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if velocity.x < speed:
		velocity.x += acceleration
